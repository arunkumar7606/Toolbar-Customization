package com.arun.aashu.toolbarcustomization;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(null);

        Toolbar toptoolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toptoolbar);
        toptoolbar.setLogo(R.mipmap.ic_launcher_round);
        toptoolbar.setLogoDescription(getResources().getString(R.string.logo_desc));
    }
}
